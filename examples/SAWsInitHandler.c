
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SAWs.h"

int main(int argc,char **argv)
{
  int         i,length             = 3;
  char        **stringData         = malloc(sizeof(char*));
  char        **stringDataCopy     = malloc(sizeof(char*));
  char        **stringDataArray    = malloc(length * sizeof(char*));
  char        **stringDataArrayCopy= malloc(length * sizeof(char*));
  int         *intData             = malloc(sizeof(int));
  int         *intDataArray        = malloc(length * sizeof(int)); 
  int         *intDataCopy         = malloc(sizeof(int));
  int         *intDataArrayCopy    = malloc(length * sizeof(int));
  double      *doubleData          = malloc(sizeof(double));
  double      *doubleDataArray     = malloc(length * sizeof(double));
  double      *doubleDataCopy      = malloc(sizeof(double));
  double      *doubleDataArrayCopy = malloc(length * sizeof(double));
  float       *floatData           = malloc(sizeof(float));
  float       *floatDataArray      = malloc(length * sizeof(float));
  float       *floatDataCopy       = malloc(sizeof(float));
  float       *floatDataArrayCopy  = malloc(length * sizeof(float));
  char        *charData            = malloc(sizeof(char));
  char        *charDataArray       = malloc(length * sizeof(char));
  char        *charDataCopy        = malloc(sizeof(char));
  char        *charDataArrayCopy   = malloc(length * sizeof(char));
  int         *boolData            = malloc(sizeof(int));
  int         *boolDataArray       = malloc(length * sizeof(int));
  int         *boolDataCopy        = malloc(sizeof(int));
  int         *boolDataArrayCopy   = malloc(length * sizeof(int));
  

  *boolData       = 1;
  *boolDataCopy   = 1;
  *intData        = 41;
  *intDataCopy    = 41;
  *doubleData     = 1.234567;
  *doubleDataCopy = 1.234567;
  *floatData      = 7.65432;
  *floatDataCopy  = 7.65432;
  *charData       = 'b';
  *charDataCopy   = 'b';
  *stringData     = strdup("string");
  *stringDataCopy = "string";
  char fullurl[256];

  for(i=0;i<length;i++){
    boolDataArray[i]       = 1;
    boolDataArrayCopy[i]   = 1;
    intDataArray[i]        = i + 12;
    intDataArrayCopy[i]    = i + 12;
    doubleDataArray[i]     = 27.7 + i;
    doubleDataArrayCopy[i] = doubleDataArray[i];
    floatDataArray[i]      = 13.3 + i;
    floatDataArrayCopy[i]  = 13.3 + i;
    charDataArray[i]       = 'b';
    charDataArrayCopy[i]   = 'b';
    stringDataArray[i]     = strdup("string");
    stringDataArrayCopy[i] = "string";
  }
  stringDataArray[length-1]     = NULL;
    
  //SAWs_Set_Port(8080);
  //SAWs_Set_Use_HTTPS("saws.pem");
  SAWs_Set_Use_Logfile(NULL);
  SAWs_Set_Document_Root("./");
  SAWs_Add_Document_Root("../");

  // SAWs look for .js scripts in local js directory, not at MCS website; see SAWs.c
  //SAWs_Push_Local_Header();
  
  SAWs_Initialize();
  SAWs_Get_FullURL(sizeof(fullurl),fullurl);
  printf("Point your browser to %s\n",fullurl);

  SAWs_Register("/example1/example3/example6/residual1",boolData,1,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Register("/example2/example3/residual2",stringData,1,SAWs_WRITE,SAWs_STRING);
  SAWs_Register("/example1/example3/example6/example7/residual3",doubleData,1,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Register("/example2/residual4",intData,1,SAWs_WRITE,SAWs_INT);
  SAWs_Register("/example1/residual5",floatData,1,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Register("/example2/_residual6",charData,1,SAWs_WRITE,SAWs_CHAR);
  SAWs_Register("/example1/residual7",boolDataArray,length,SAWs_WRITE,SAWs_BOOLEAN);
  SAWs_Register("/example1/example3/residual8",stringDataArray,length,SAWs_WRITE,SAWs_STRING);
  SAWs_Register("residual9",doubleDataArray,length,SAWs_WRITE,SAWs_DOUBLE);
  SAWs_Register("/example1/example3/residual10",intDataArray,length,SAWs_WRITE,SAWs_INT);
  SAWs_Register("/example1/example4/residual11",floatDataArray,length,SAWs_WRITE,SAWs_FLOAT);
  SAWs_Register("/example6/residual12",charDataArray,length,SAWs_WRITE,SAWs_CHAR);
  

  /*Change to while to see the updating information or to getchar() to allow a finishing run */

  //while(1){
  //(*intData)++;
  //}

  getchar();

  SAWs_Delete("/example1/example3/example6/example7/residual3");
  SAWs_Delete("/example2/example3/");
  //SAWs_Destroy_Directory(&amem);

  SAWs_Finalize();

  /*free all the user data*/
  free(intData);
  free(intDataArray);
  free(intDataCopy);
  free(intDataArrayCopy);
  free(*stringData);
  free(stringData);
  free(stringDataCopy);
  free(stringDataArrayCopy);
  for(i=0;i<length;i++){
    free(stringDataArray[i]);
  }
  free(stringDataArray);
  free(doubleData);
  free(doubleDataArray);
  free(doubleDataArrayCopy);
  free(doubleDataCopy);
  free(floatData);
  free(floatDataArray);
  free(floatDataCopy);
  free(floatDataArrayCopy);
  free(charData);
  free(charDataArray);
  free(charDataCopy);
  free(charDataArrayCopy);
  free(boolData);
  free(boolDataArray);
  free(boolDataCopy);
  free(boolDataArrayCopy);

  return 0;

}
